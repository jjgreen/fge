% sample.tex 
% sample usage for the fge font
% J.J. Green 2005

\documentclass[11pt]{article}
\usepackage{fge}
\usepackage[8]{bguq}
\usepackage[bguq]{begriff1207}

\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[pdftex]{graphicx}

\setlength{\BGthickness}{0.8pt}
\setlength{\BGbeforelen}{2pt}
\setlength{\BGafterlen}{2pt}

\newcommand{\iGamma}{\mathit{\Gamma}}
\newcommand{\iDelta}{\mathit{\Delta}}
\newcommand{\iSigma}{\mathit{\Sigma}}
\newcommand{\iPhi}{\mathit{\Phi}}
\newcommand{\fraka}{\mathfrak{a}}
\newcommand{\frake}{\mathfrak{e}}
\newcommand{\frakg}{\mathfrak{g}}
\newcommand{\fraku}{\mathfrak{u}}

\newcommand{\Vol}[1]{Vol.\thinspace#1}
\newcommand{\Sec}[1]{\S#1}
\newcommand{\Page}[1]{p.\thinspace#1}
\newcommand{\q}[3]{\Vol{#1}, \Sec{#2}, \Page{#3}}
\newcommand{\qepi}[2]{\Vol{#1}, Epilogue, \Page{#2}}

\title{Erratics for Frege's \textit{Grundgesetze der Arithmetik}}
\author{J.J. Green}
\date{March 2006\thanks{%
    Updated July 2012 to used improved \textit{Begriffsschrift} 
    quantifier from the \texttt{bguq} package}}

\begin{document}
\maketitle

This document compares some of the unusual characters used as 
notation by G.~Frege in \textit{Grundgesetze der Arithmetik} 
with their reimplementations in the \texttt{fge} font.

The \textit{Grundgesetze} was published in two volumes in 1893 and
1903 by Julius Hermann Pohle of Rathenaustrasse 2, Jena and was 
the culmination of Frege's logicist project. The first complete 
English translation is to be published by the Oxford Univerity
Press.  

In comparing the characters we present photographs from a first printing 
alongside the same text set in Computer Modern at 11pt. These mostly 
show part of a two column layout that is challenging to justify, so for 
the reconstruction we override \TeX's justification rules by
manually inserting the line-breaks of the original. 

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=120mm]{../scans/para/1-010-019}}
    \linebreak
    \fbox{
      \newcommand{\frm}{,$\fgebackslash\spirituslenis{\varepsilon}\iPhi(\varepsilon)$`}
      \begin{minipage}{119mm}
        \begin{small}
          \sloppy
          \hspace{20pt}Danach ist 
          $\fgebackslash\spirituslenis{\varepsilon}(\iDelta=\varepsilon)=\iDelta$
          das Wahre, und es bedeutet \frm\ dann\linebreak
          den unter den Begriff $\iPhi(\xi)$ fallenden Gegenstand, wenn 
          $\iPhi(\xi)$ ein Begriff\linebreak
          ist, unter den ein und nur ein Gegenstand f\"{a}llt; in allen 
          andern F\"{a}llen\linebreak
          bedeutet \frm\ dasselbe wie ,$\spirituslenis{\varepsilon}\iPhi(\varepsilon)$'.  
          So ist z.~B.~$2=\fgebackslash\spirituslenis{\varepsilon}(\varepsilon+3=5)$ 
          das\linebreak
          Wahre, weil 2 der einzige Gegenstand ist, der unter den Begriff
        \end{small}
      \end{minipage}
    }
  \end{center}
  \caption{The reverse solidus, $\fgebackslash$, \q{1}{11}{19}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=32mm]{../scans/para/1-122-150}}
    \linebreak
    \fbox{
      \begin{minipage}{32mm}
        $\BGdefvar{1mm}
        \fgelb(\fgestruckzero \fgecap \fgeU \fgecupacute \fges) = \fgeinfty$
      \end{minipage}
    }
  \end{center}
  \caption{The overturned lb (pound) ligature, $\fgelb$; the struckout zero, 
    $\fgestruckzero$; the mark (currency) sign, $\fgeU$; the medial s, $\fges$ 
    and the infinity sign, $\fgeinfty$, \q{1}{122}{150}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 40 0 0,clip,width=68mm]{../scans/para/1-144-180}}
    \linebreak
    \fbox{
      \begin{minipage}{65mm}
        \setlength{\baselineskip}{9pt}
        \sloppy
        ist leicht aus dem Satze
        \[
        \BGassert
        \BGconditional{
          n \fgecap (y \fgecap ( m; x \fgelangle (p \fgecupbar q)))
        }
        {
          x \fgecap (y \fgecap \fgecupacute q)
        }
        \]
        abzuleiten, und wir k\"{o}nnen damit
      \end{minipage}
    }
  \end{center}
  \caption{The left angle, $\fgelangle$, \q{1}{144}{180}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{
      \begin{minipage}{20mm}
        \includegraphics[trim=0 0 0 0,clip,width=20mm]{../scans/para/1-172-225}
      \end{minipage}
    }
    %\linebreak
    \fbox{
      \begin{minipage}{26mm}
        \setlength{\baselineskip}{8pt}
        \sloppy
        $
        \begin{array}{rcc}\displaystyle
          n & \underset{p}{\fgerightarrow} & y \\
          \mbox{\small $\fgecupacute q$}\,\,\fgeuparrow & & \\
          a & \underset{p}{\fgerightarrow} & c \\
          \mbox{\small $\fgecupacute q$}\,\,\fgeuparrow & & \\
          m & \underset{p}{\fgerightarrow} & x 
        \end{array}
        $
      \end{minipage}
    }
  \end{center}
  \caption{The fletched arrows $\fgeuparrow$ and $\fgerightarrow$, \q{1}{172}{225}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=70mm]{../scans/para/2-001-002}}
    \linebreak
    \fbox{
      \begin{minipage}{70mm}
        \setlength{\baselineskip}{9pt}
        \sloppy
        \begin{center}
          $
          \BGdefvar{1mm}
          \spirituslenis{\alpha}
          \spirituslenis{\varepsilon}
          \BGbracket{
            \BGnot
            \BGconditional{
              \varepsilon \fgecap 
              (\alpha \fgecap (v \fgecapbar \fgecupacute q)) 
            }
            {
              \varepsilon \fgecap 
              (\alpha \fgecap ( v \fgecapbar \fgecupacute q \fgeupbracket \fgebaracute q ))
            }
          }
          = {v \fgee q}
          % remark: without the braces in the line above the spacing
          % is too tight, because the binary-operatorness of \fgee is
          % overrridden by that of the equality.
          $
        \end{center}
      \end{minipage}
    }
  \end{center}
  \caption{The overturned e with spur, $\fgee$, \q{2}{1}{2}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=67mm]{../scans/para/2-007-007}}
    \linebreak
    \fbox{
      \begin{minipage}{65mm}
        \sloppy
        Glieder der mit $m$ anfangenden $(v\fgec q)$-
      \end{minipage}
    }
  \end{center}
  \caption{The overturned c with spur, $\fgec$, \q{2}{7}{7}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=122mm]{../scans/para/2-083-094}}
    \linebreak
    \fbox{
      \begin{minipage}{119mm}
        \begin{small}
          \sloppy
          \hspace{17pt},,Jetzt erst kommen die Definitionen des 
          Azig-, Bezig-, und Zezigseins\linebreak
          zweier Zahlen $b$ und $b'$, und zwar sagt 
          man, dass $b\fgeleftthree b'$ oder $b\fgelefttwo b'$ oder\linebreak
          $b\fgerighttwo b'$ ist, jenachdem $b\fgerightB b'$ azig Poll oder bezig Poll
          oder zezig Poll ist\lq\lq\ $\left.^2\right)$.
        \end{small}
      \end{minipage}
    }
  \end{center}
  \caption{Rotated characters and numerals, \q{2}{83}{94}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=90 0 0 0,clip,width=35mm]{../scans/para/2-198-191}}
    \linebreak
    \fbox{
      \begin{minipage}{34mm}
        \setlength{\baselineskip}{9pt}
        \sloppy
        $
        \setlength{\BGlinewidth}{17mm}
        \BGassert
        \BGconditional
        {\BGterm{\fged s}}
        {\BGnot\BGconditional
          {\BGquant{\fraka}\BGconditional
            {\BGquant{\frake}\BGconditional
              {\BGterm{\frake\fgecap s}}
              {\BGnot\BGterm{\fraka\fgeupbracket\fgeU\frake\fgecap s}}
            }  
            {\BGnot\BGterm{\fraka\fgecap s}}
          }
          {\BGnot\BGterm{\fgef s}}
        }
        $
      \end{minipage}
    }
  \end{center}
  \caption{The overturned and hooked d and f, $\fged$ and $\fgef$ \q{2}{198}{191}}
\end{figure}

\begin{figure}
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 60 0,clip,width=55mm]{../scans/para/2-201-193}}
    \linebreak
    \fbox{
      \begin{minipage}{47mm}
        \setlength{\baselineskip}{9pt}
        \begin{center}
          $
          \setlength{\BGlinewidth}{4mm}
          \BGassert
          \BGconditional
          {\BGterm{c\fgecap(s\,\text{\l}\,(p\fgecap(s\fgeeszett p)))}}
          {\BGconditional
            {\BGterm{p \fgecap s}}
            {\BGterm{c\fgeupbracket p\fgecap 
                (s\,\text{\l}\,(p\fgeupbracket p\fgecap(s\fgeeszett p)))}}
          }
          $
        \end{center}
      \end{minipage}
    }
  \end{center}
  \caption{The overturned \ss, $\fgeeszett$, \q{2}{201}{193}}
\end{figure}

\begin{figure}
  % this needed for an "inner content stroke", we can't use
  % the \BGcontent since this modifies the global \BGlinewidth
  \newcommand{\BGinnercontent}[0]{%
    \vrule depth 0pt height \BGthickness width 10pt%
    \hskip \BGspace%
  }
  \begin{center}
    \setlength{\fboxsep}{5pt}
    \setlength{\fboxrule}{0pt}
    \fbox{\includegraphics[trim=0 0 0 0,clip,width=38mm]{../scans/para/2-epi-256}}
    \linebreak
    \fbox{
      \begin{minipage}{45mm}
        \setlength{\baselineskip}{9pt}
        \begin{center}
          $
          \setlength{\BGlinewidth}{7mm}
          \BGconditional
          {\BGquant{\frakg}\BGconditional
            {\BGterm{\spirituslenis{\varepsilon}(\BGinnercontent\frakg(\varepsilon))=\fgeA}}
            {\BGterm{\frakg(\fgeA)}}}
          {\BGconditional
            {\BGterm{\spirituslenis{\varepsilon}(\BGinnercontent f(\varepsilon))=\fgeA}}
            {\BGterm{f(\fgeA)}}}
          $
        \end{center}
      \end{minipage}
    }
  \end{center}
  \caption{The overturned A, $\fgeA$, \qepi{2}{256}}
\end{figure}

\end{document}
