CHANGES

1.25 : 19/05/15

- Added the Spiritus Asper, used by Frege in his letter to
  Bertrand Russell of 29 June 1902 denoting a predicate true
  of all unary functions of objects (thanks to Ansten Klev)
- Fixed an error in the transcription of the Spiritus Lenis
  from the Ibycus font.

1.24 : 17/11/11

- Renamed the "strange U" from \fgeU to \fgemark after
  the discovery that the symbol is a archaic Mark 
  (currency) symbol: see

     http://mathoverflow.net/questions/74574/

  The older name will be retained indefintely as an alias 
  for reasons of compatibility with older documents.

1.23 : 06/05/09

- Fixed the map-file fge.map, now dvips output is correct
  (many thanks to Graham R. Shutt for the bug report)

1.22 : 27/02/09

- Fixed some defects in the bowl and serif of the rotated
  eszett.

1.21 : 03/06/07

- Reinstated the crescent form of the spritus lenis, accessed
  using the new "crescent" option

1.20 : 04/04/07

- Improved fletching on the up- and right-arrows. 

1.19 : 28/02/07

- Discovered that the overturned U is, in fact, an overturned
  lb (pound) ligature, and so have renamed \fgeoverU to 
  \fgeLb (the previous \fgeN, \fgeeta, and \fgeoverU are
  retained)
- Minor documentation fixes.

1.18 : 20/02/07

- Worked through the adjust_fit settings for the characters.
  - Added some extra space around the cup & cap series
  - Added a little to the overturned U
  - Moved the spacing on the eszett from the left to the
    right (since it has been rotated)
- Removed the mid-bar on the rotated eszett (it is present in
  the Computer Modern form, but not in the original).
- Added slope to the right serif on the foot of the overturned U
  (as done with the overturned d with hook) for conssitency with 
  Computer Modern.
- Code cleanup.

1.17 : 18/02/07

- Adjusted the italic corrections of the characters in fgeit
  according to Knuth on p105 of the Metafont Book.
- Overturned & hooked f and d are of \mathord type rather than
  \mathbin. The \fgeeta has been renamed \fgeoverU, having 
  realised that it is an overturned U. The macro \fgeeta
  is an alias for \fgeoverU but is deprecated.

1.16 : 29/01/07

- Added the overturned A, \fgeA, which denotes the Russell
  class: the class of all classes that do are not member of 
  themselves.

1.15 : 25/01/07

- Replaced the (rather weak) spiritus lenis with a version
  derived from the ibycus font for classical Greek by 
  Pierre A. MacKay, from work by Silvio Levy's realization 
  of a classic Didot cut of Greek type from around 1800.

1.14 : 23/01/07

- Removed \fged, which is done better done in the tipa font.
- Added \fgef, \fged, \fegeszett.
- Added bgquant.sty, prettier quantifiers for begriff.sty

1.13 : 02/01/07

- Corrected some installation problems

1.12 : 30/12/06

- Added the rotated 2, 3, B and C. 
- Tidied fgebase.mf.
- Modified \fgeeta to use the CM serif, rather than our
  custom version.

1.11 : 01/12/06

- Added the stuckout numerals 0 and 1, and fletched
  arrows.

1.10 : 25/11/06

- Added the reverse solidus, \fgebackslash, capital-height
  and roman-stressed in contrast to the cm \backslash

1.09 : 23/11/06

- Recut \fgeeta as an italic -- the new version has the
  italic slant of computer modern. The old version is still
  available but is now named \fgevareta. Will probably be 
  removed (it has a broken foot).

1.08 : 21/11/06

- Several cup & cap characters

1.07 : 19/11/06

- Added inverted F, \fgeF

1.06 :

- Split font into roman (fgerm) and italic (fgeit)
- Added italic d-bar, \fged

1.05 : 10/07/06

- Long s with mid-bar, \fges

1.04 : 07/07/06

- Overturned e with spur, \fgee

1.03 : 07/06/06

- Added overturned c with spur, \fgec
- The source-code is now spread over several files, and
  also inputs the Computer Modern macros (cmbase.mf).

1.02 : 29/05/06

- Added the left angle, \fgelangle
- Renamed \fgeN to \fgeeta 

1.01 : 04/04/06 

- Added the spiritus lenis math accent

