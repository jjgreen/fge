Install automatically
---------------------

For Unix: installs into /usr/local/share/texmf, if you want it 
elsewhere then edit the Makefile. Run

  make install

as root

Install manually
----------------

In the following replace $(TEXMF) by your TeX tree (where TeX 
looks for its files -- this will vary depending on your TeX 
distribution, often /usr/local/share/texmf on Unix systems)

Create directories

   mkdir $(TEXMF)/fonts/source/fge/
   mkdir $(TEXMF)/fonts/tfm/fge/
   mkdir $(TEXMF)/tex/latex/fge/

and copy files

   cp  *.mf  $(TEXMF)/fonts/source/fge/
   cp  *.tfm $(TEXMF)/fonts/tfm/fge/
   cp  fge.sty $(TEXMF)/tex/latex/fge/
   cp  Ufgerm.fd $(TEXMF)/tex/latex/fge/
   cp  Ufgeit.fd $(TEXMF)/tex/latex/fge/

Your TeX distribution may expect you to update its cache
of files -- if using TeXLive or tetex run

  texhash

Installing the PostScript files depends on your TeX distribution
and operating system. For TeXLive on Debian/Ubuntu

  cp *.pfb $(TEXMF)/fonts/type1/fge/
  cp fge.map $(TEXMF)/fonts/map/dvips/
  cp fge.cfg /etc/texmf/updmap.d/20fge.cfg

then run

  update-updmap
  mktexlsr
  updmap-sys 


Testing
-------

To test, run

  latex fge.dtx

and view the file fge.dvi, then

  pdflatex fge.dtx

and view fge.pdf

