# Makefile for fge

# the base of your TeX distribution's tree

TEXMF = /usr/local/share/texmf

# you probably dont want to change anything below

NAME   = fge
NAMERM = $(NAME)rm
NAMEIT = $(NAME)it
SIZE = 10
FONTRM = $(NAMERM)$(SIZE)
FONTIT = $(NAMEIT)$(SIZE)

# the fres is the resolution of the mode times the 
# magstep. dres is the resolution of the printer :
# ljfive is my printer, 600 dpi.

MAG  = 4
MODE = ljfive
DRES = 600
FRES = 2400

DVIPS  = dvips -m$(MODE) -Pcmz -Pamz

PS2PDF = ps2pdf

MFBASE = $(NAME)base.mf

MFRM  = $(NAMERM).mf $(FONTRM).mf
GFRM  = $(FONTRM).$(FRES)gf   
TFMRM = $(FONTRM).tfm

MFIT  = $(NAMEIT).mf $(FONTIT).mf
GFIT  = $(FONTIT).$(FRES)gf   
TFMIT = $(FONTIT).tfm

MF    = $(MFRM) $(MFIT) $(MFBASE)
GF    = $(GFRM) $(GFIT)
TFM   = $(TFMRM) $(TFMIT)

PFBIT = $(FONTIT).pfb
PFBRM = $(FONTRM).pfb
PFB   = $(PFBIT) $(PFBRM)
MAP   = $(NAME).map
CFG   = $(NAME).cfg

INS  = $(NAME).ins
DTX  = $(NAME).dtx
ENC  = U
FDRM = $(ENC)$(NAMERM).fd
FDIT = $(ENC)$(NAMEIT).fd
FD   = $(FDRM) $(FDIT)
STY  = $(NAME).sty

PKRM = $(FONTRM).*pk
PKIT = $(FONTIT).*pk
PK   = $(PKRM) $(PKIT)

RUBBISH += *.log *.aux *~

default : all

all : $(TFM) $(FDRM) $(FDIT) $(STY) $(PFB)

# the font

PRFRM    = $(FONTRM).dvi 
PRFRMPS  = $(FONTRM).ps
PRFRMPDF = $(NAMERM)-proof.pdf 

PRFIT    = $(FONTIT).dvi 
PRFITPS  = $(FONTIT).ps
PRFITPDF = $(NAMEIT)-proof.pdf 

PRF      = $(PRFRM) $(PRFIT)
PRFPS    = $(PRFRMPS) $(PRFITPS)
PRFPDF   = $(PRFRMPDF) $(PRFITPDF)

RUBBISH += $(GF) $(PRF) $(PRFPS) $(PRFPDF) 

MKGFTFM = mf '\mode=$(MODE); \mag=$(MAG);' 

$(GF) $(TFM) : $(MF)
	$(MKGFTFM) input $(FONTRM).mf 
	$(MKGFTFM) input $(FONTIT).mf 

MTARG = --formats=pfb --magnification=2000 --simplify

$(PFBRM) : $(MF)
	mftrace $(MTARG) $(FONTRM)

$(PFBIT) : $(MF)
	mftrace $(MTARG) $(FONTIT)

RUBBISH +=  slantfge.600pk slantfge.tfm

slantfge.tfm : slantfge.mf
	$(RM) slantfge.tfm
	mktextfm slantfge

$(PRFRM) : $(GFRM) $(TFMRM) slantfge.tfm 
	gftodvi -verbose $(GFRM)

$(PRFIT) : $(GFIT) $(TFMIT) slantfge.tfm 
	gftodvi -verbose $(GFIT)

$(PRFRMPDF) : $(PRFRMPS)
	$(PS2PDF) $(PRFRMPS) $(PRFRMPDF) 

$(PRFITPDF) : $(PRFITPS)
	$(PS2PDF) $(PRFITPS) $(PRFITPDF) 

show-proof-rm : $(PRFRM) 
	xdvi -s 5 $(PRFRM)

show-proof-it : $(PRFIT)
	xdvi -s 5 $(PRFIT)

# supporting packages

DOCPDF   = $(NAME)-doc.pdf
RUBBISH += $(DOCPDF) $(PK)

$(FD) $(STY) : $(DTX)
	$(RM) $(FD) $(STY)
	latex $(INS)

$(DOCPDF) : $(FD) $(STY) $(DTX) $(PFB)
	pdflatex $(NAME).dtx
	mv $(NAME).pdf $(DOCPDF)

show-doc : $(DOCPDF) 
	acroread $(DOCPDF) 

install : install-mf install-tex install-ps

install-mf : $(MF)
	install -d $(TEXMF)/fonts/source/fge
	for file in $(MF) ; do \
	  install -m 644 $$file $(TEXMF)/fonts/source/fge/$$file ;\
	done

install-tex : $(TFM) $(FD) $(STY)
	install -d $(TEXMF)/fonts/tfm/fge
	for file in $(TFM) ; do \
	  install -m 644 $$file $(TEXMF)/fonts/tfm/fge/$$file ;\
	done
	install -d $(TEXMF)/tex/latex/fge
	for file in $(FD) $(STY) ; do \
	  install -m 644 $$file $(TEXMF)/tex/latex/fge/$$file ;\
	done

# this is probably debian specific

install-ps : $(PFB) $(MAP) $(CFG)
	install -m 644 $(CFG) /etc/texmf/updmap.d/20$(CFG)
	install -d $(TEXMF)/fonts/map/dvips/
	install -m 644 $(MAP) $(TEXMF)/fonts/map/dvips/$(MAP)
	install -d $(TEXMF)/fonts/type1/fge
	for file in $(PFB) ; do \
	  install -m 644 $$file $(TEXMF)/fonts/type1/fge/$$file ;\
	done
	update-updmap
	mktexlsr
	updmap-sys 

# add ps targets too

uninstall : 
	for file in $(MF) ; do $(RM) $(TEXMF)/fonts/source/fge/$$file ; done
	for file in $(TFM) ; do $(RM) $(TEXMF)/fonts/tfm/fge/$$file ; done
	for file in $(FD) $(STY) ; do $(RM) $(TEXMF)/tex/latex/fge/$$file ; done

CTAN = $(MF) $(TFM) $(INS) $(DTX) $(DOCPDF) $(PFB) $(MAP) $(CFG)

RUBBISH += fge.zip

fge.zip : $(CTAN)
	install -d fge
	for file in $(CTAN) ; do install -m 644 $$file fge/$$file ; done
	install -m 644 README.ctan fge/README
	zip -r fge fge
	rm fge/*
	rmdir fge

ctan : fge.zip

ctan-clean :
	$(RM) fge/* fge.zip

# eps files of scans

SCAN     = ../scans/para
EPS      = eps
RUBBISH += $(EPS)/*.eps

scans-eps :
	for file in `ls -1 $(SCAN)/*.jpg` ; do \
	  jpeg2ps -o eps/`basename $$file .jpg`.eps $$file ;\
	done

# sample - this requires some scans which are not included
# in the distribution (much too big)

RUBBISH += fge-sample.pdf

fge-sample.pdf : sample.tex $(FD) $(STY) $(TFM)
	pdflatex sample
	mv sample.pdf fge-sample.pdf

show-sample : fge-sample.pdf 
	acroread fge-sample.pdf

# sltest is a test for the spiritus lenis

RUBBISH += sltest.dvi sltest.ps fge-sltest.pdf

sltest.dvi : sltest.tex $(FD) $(STY) $(TFM)
	latex sltest

show-sltest : sltest.dvi 
	xdvi -s 5 sltest

fge-sltest.pdf : sltest.ps
	$(PS2PDF) sltest.ps fge-sltest.pdf 

# ittest is a test for the math italic

RUBBISH += ittest.dvi ittest.ps fge-ittest.pdf

ittest.dvi : ittest.tex $(FD) $(STY) $(TFM)
	latex ittest

show-ittest : ittest.dvi 
	xdvi -s 5 ittest

fge-ittest.pdf : ittest.ps
	$(PS2PDF) ittest.ps fge-ittest.pdf 

# ittest is a test for the math italic

RUBBISH += rmtest.dvi rmtest.ps fge-rmtest.pdf

rmtest.dvi : rmtest.tex $(FD) $(STY) $(TFM)
	latex rmtest

show-rmtest : rmtest.dvi 
	xdvi -s 5 rmtest

fge-rmtest.pdf : rmtest.ps
	$(PS2PDF) rmtest.ps fge-rmtest.pdf 

# other targets

clean :
	$(RM) $(RUBBISH)

# suffix rules

.SUFFIXES : .pdf .ps .dvi

.dvi.ps :
	$(DVIPS) -o $@ $<

.ps.pdf :
	$(PS2PDF) $< $@

